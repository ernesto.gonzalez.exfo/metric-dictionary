require_relative 'rest_api'
require 'cgi'

class FileCommitsApi < RestApi
  PROJECT_PATH = "gitlab-org/gitlab"

  def initialize(file_path)
    @file_path = file_path
  end

  private

  attr_reader :file_path

  def path
    "/projects/#{encoded_project_path}/repository/commits"
  end

  def encoded_project_path
    CGI.escape(PROJECT_PATH)
  end

  def params
    {
      path: file_path
    }
  end
end
