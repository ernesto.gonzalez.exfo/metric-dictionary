require_relative 'api'

class TreeApi < Api
  def initialize(path)
    @path = path
  end

  private

  def parse(json)
    json.dig('data', 'project', 'repository', 'tree', 'blobs', 'nodes')
  end

  def page_info_path
    ['project', 'repository', 'tree', 'blobs']
  end

  def params
    @path
  end

  def query
    <<-gql
    {
      project(fullPath: "gitlab-org/gitlab") {
        id
        repository {
          tree(path: "%{params}") {
            blobs(after: "%{after}"){
              nodes{
                id
                path
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
        }
      }
    }
    gql
  end
end
