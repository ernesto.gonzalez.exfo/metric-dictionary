require "date"
require "uri"

class MetricRow
  SERIALIZED_ATTRIBUTES = [:time_frame, :data_source, :data_category, :status, :product_stage,
    :product_section, :product_group, :milestone, :instrumentation_class, :key_path, :web_path,
    :tier, :description, :introduced_by_url, :value_type, :autoloaded_introduced_by_url, :options]
  SERIALIZED_METHODS = [:date_created, :performance_indicator_type, :tier, :distribution, :sisense_query, :instrumentation_class_link]

  GITLAB_BASE_URL = "https://gitlab.com/gitlab-org/gitlab/-/blob/master/"
  INSTRUMENTATION_PATH = "lib/gitlab/usage/metrics/instrumentations/"
  INTRODUCED_BY_URL_KEY = "introduced_by_url"
  AUTOLOADED_INTRODUCED_BY_URL_KEY = "autoloaded_introduced_by_url"

  attr_reader *SERIALIZED_ATTRIBUTES

  def initialize(data)
    @data = data

    @time_frame = data["rawBlob"]["time_frame"]
    @data_source = data["rawBlob"]["data_source"]
    @data_category = data["rawBlob"]["data_category"]
    @status = data["rawBlob"]["status"]
    @product_stage = data["rawBlob"]["product_stage"]
    @product_section = data["rawBlob"]["product_section"]
    @product_group = data["rawBlob"]["product_group"]
    @milestone = data["rawBlob"]["milestone"]
    @key_path = data["rawBlob"]["key_path"]
    @description = data["rawBlob"]["description"]
    @instrumentation_class = data["rawBlob"]["instrumentation_class"]
    @introduced_by_url = data["rawBlob"][INTRODUCED_BY_URL_KEY]
    @autoloaded_introduced_by_url = data[AUTOLOADED_INTRODUCED_BY_URL_KEY]
    @value_type = data["rawBlob"]["value_type"]
    @web_path = data["webPath"]
    @options = data["rawBlob"]["options"]
  end

  def to_json(*args)
    SERIALIZED_ATTRIBUTES.dup.concat(SERIALIZED_METHODS).map { |method| [method.to_s, public_send(method)] }.to_h.to_json(*args)
  end

  def date_created
    date = @data['name'].match(/^(\d)*/)[0]
    Date.parse(date).to_s
  end

  def performance_indicator_type
    @data['rawBlob'].fetch('performance_indicator_type', []).join(', ')
  end

  def distribution
    @data['rawBlob'].fetch('distribution', []).join(', ')
  end

  def tier
    tier = @data['rawBlob']['tier']
    if tier.include?('free')
      'free'
    elsif tier.include?('premium') &&  tier.include?('ultimate')
      'premium'
    else
      'ultimate'
    end
  end

  def sisense_query
    <<-SQL
      SELECT
        ping_created_at,
        metrics_path,
        metric_value,
        has_timed_out --if metric timed out, the value will be set to 0
      FROM common.fct_ping_instance_metric_rolling_6_months --model limited to last 6 months for performance
      WHERE dim_instance_id = 'ea8bf810-1d6f-4a6a-b4fd-93e8cbd8b57f' --SaaS instance, both production and staging installations
      AND metrics_path = \'#{key_path}\' --set to metric of interest
      ORDER BY ping_created_at DESC
      LIMIT 5
      ;
    SQL
  end

  def instrumentation_class_link
    return nil if instrumentation_class.nil?
    URI.join(GITLAB_BASE_URL, ee_or_ce_path, INSTRUMENTATION_PATH, instrumentation_class_file).to_s
  end

  private

  def ee_or_ce_path
    @data['rawBlob'].fetch('distribution', []).include?("ce") ? "" : "ee/"
  end

  def instrumentation_class_file
    class_name = @instrumentation_class
    return '' if class_name.nil? || class_name.empty?

    file_name = class_name.split("::").map do |part|
      part.gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
      .gsub(/([a-z\d])([A-Z])/, '\1_\2')
      .downcase
    end.join("/")

    file_name.concat(".rb")
  end
end
