# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/builder"

RSpec.describe Builder do
  let(:instance) { described_class.new }
  let(:tree_api_response) do
    [
      {
        "path" => "20210101_metric_name_1.yml"
      }, {
        "path" => "20210104_metric_name_2.yml"
      }
    ]
  end
  let(:blob_api_response) do
    [
      {
        "rawBlob" => "tier: free\nkey_path: metric_name_1\nname: metric_name_1",
        "name" => "20210101_metric_name_1.yml"
      }, {
        "rawBlob" => "tier: free\nkey_path: metric_name_2\nname: metric_name_2",
        "name" => "20210104_metric_name_2.yml"
      }
    ]
  end
  let(:file_commits_api1_response) do
    [
      {
        "id" => "sha_1"
      },
      {
        "id" => "sha_2"
      }
    ]
  end
  let(:file_commits_api2_response) do
    [
      {
        "id" => "sha_3"
      },
      {
        "id" => "sha_4"
      }
    ]
  end
  let(:commit_mrs_api1_response) do
    [
      {
        "id" => "mr_id_1",
        "web_url" => "mr_web_url"
      }
    ]
  end
  let(:commit_mrs_api2_response) { [] }

  before do
    stub_const("Loader::SERVICE_PING_METRIC_FOLDERS", ["metrics"])

    tree_api = double(TreeApi)
    allow(TreeApi).to receive(:new).and_return(tree_api)
    allow(tree_api).to receive(:fetch).and_return(tree_api_response)

    blob_api = double(BlobApi)
    allow(BlobApi).to receive(:new).and_return(blob_api)

    # the response needs to be invoked 4 times: 2 times because we have CE/EE folders, * 2 because we have service_ping + snowplow loading
    responses = 4.times.map { blob_api_response.map(&:dup) }
    allow(blob_api).to receive(:fetch).and_return(*responses)

    file_commits_api1 = double(FileCommitsApi)
    file_commits_api2 = double(FileCommitsApi)

    allow(FileCommitsApi).to receive(:new) do |file_path|
      next file_commits_api1 if file_path.end_with? "20210101_metric_name_1.yml"

      file_commits_api2 if file_path.end_with? "20210104_metric_name_2.yml"
    end

    allow(file_commits_api1).to receive(:get).and_return(file_commits_api1_response)
    allow(file_commits_api2).to receive(:get).and_return(file_commits_api2_response)

    commit_mrs_api1 = double(CommitMergeRequestsApi)
    commit_mrs_api2 = double(CommitMergeRequestsApi)

    allow(CommitMergeRequestsApi).to receive(:new) do |sha|
      next commit_mrs_api1 if sha == "sha_2"

      commit_mrs_api2 if sha == "sha_4"
    end

    allow(commit_mrs_api1).to receive(:get).and_return(commit_mrs_api1_response)
    allow(commit_mrs_api2).to receive(:get).and_return(commit_mrs_api2_response)
  end

  after do
    FileUtils.rm_f('service_ping_data.json')
    FileUtils.rm_f('event_data.json')
  end

  it "builds csv data correctly" do
    instance.build

    expected_data = [
      ["key_path", "name", "tier"],
      ["metric_name_1", "metric_name_1", "free"],
      ["metric_name_2", "metric_name_2", "free"],
      ["metric_name_1", "metric_name_1", "free"],
      ["metric_name_2", "metric_name_2", "free"]
    ]
    expect(CSV.read("web/static/data/service_ping.csv")).to eq(expected_data)
    expect(CSV.read("web/static/data/snowplow.csv")).to eq(expected_data)
  end

  it "builds json data correctly" do
    instance.build

    manifest = JSON.parse(File.read("web/static/data/manifest.json"))

    service_ping_data_template = {
      "date_created" => "2021-01-01",
      "key_path" => "metric_name_1",
      "autoloaded_introduced_by_url" => "mr_web_url",
      "tier" => "free"
    }
    service_ping_data_template2 = service_ping_data_template.merge(
      "date_created" => "2021-01-04",
      "key_path" => "metric_name_2",
      "autoloaded_introduced_by_url" => nil
    )
    expected_service_ping_data = [
      a_hash_including(service_ping_data_template),
      a_hash_including(service_ping_data_template2),
      a_hash_including(service_ping_data_template),
      a_hash_including(service_ping_data_template2)
    ]
    service_ping_json = JSON.parse(File.read("web/static/data/service_ping.#{manifest['service_ping']}.json"))
    expect(service_ping_json).to match(expected_service_ping_data)

    snowplow_data_template = { "key" => "20210101_metric_name_1", "tier" => "ultimate" }
    snowplow_data_template2 = snowplow_data_template.merge("key" => "20210104_metric_name_2")
    expected_snowplow_data = [
      a_hash_including(snowplow_data_template),
      a_hash_including(snowplow_data_template2),
      a_hash_including(snowplow_data_template),
      a_hash_including(snowplow_data_template2)
    ]
    snowplow_json = JSON.parse(File.read("web/static/data/snowplow.#{manifest['snowplow']}.json"))
    expect(snowplow_json).to match(expected_snowplow_data)
  end
end
